#!/usr/bin/perl

use warnings;
use strict;
use File::Find;
use File::Basename;
use File::Path qw(make_path);
use Image::Magick;

my $image = Image::Magick->new;

my @thumbnailable_extensions = qw(
.gif
.jpg
.jpeg
.png
);

my $timestart = time;

my $home_path = '/home/thundergoblin';
@ARGV = ("$home_path/b.robnugen.com") unless @ARGV;

my ($num_exist, $num_to_create) = (0, 0);

sub urlifyFileName {
    my ($sourceImagePath) = @_;
    # create a (non https) URL for the image
    $sourceImagePath =~ s|$home_path|http:/|;
    print "$sourceImagePath\n";
}

sub makeThumbs {
    my ($sourceImagePath, $thumbImagePath) = @_;

    urlifyFileName($sourceImagePath);

    $image = Image::Magick->new;
    open(IMAGE, $sourceImagePath);
    $image->Read(file=>\*IMAGE);
    close(IMAGE);
    
    $image->Resize(geometry=>'200x200,10,10');
    
    open(IMAGE, ">$thumbImagePath");
    $image->Write(file=>\*IMAGE, filename=>$thumbImagePath);
    close(IMAGE);
}

sub processFiles {
    # By my convention,
    # the second to last part of path is thumbs if it's a thumbnail image
    my @parts = split("/",$File::Find::name);

    # don't create thumbs in ai-cache directory
    return if($parts[4] && $parts[4] =~ m/ai-cache/);

    ## OS X .DS_Store files are useless on this server
    ## some ._.DS_Store files were created as well and are doubly useless
    if($parts[-1] =~ m/^\.(_.)?DS_Store$/) {
        unlink($File::Find::name);
##	print "deleted " . $File::Find::name. "\n";
	return;
    }

    return if($parts[-2] =~ m/thumbs/);       # don't need to make sure images in any /thumbs directory have thumbnails

    if($parts[-1] =~ m/^\._/) {
##	print "not ignoring (to late to consider deleting): " . $File::Find::name. "\n";
        unlink($File::Find::name);
	return;
    }

    # check if the file is an image file that we'd like to thumbnail
    my $pattern_match_extensions = join('|',@thumbnailable_extensions);

    if ( basename($File::Find::name) =~ /($pattern_match_extensions)\Z/i ) {
	my $thumbnaildir = dirname($File::Find::name) . "/thumbs/";
	my $thumbnailfile = $thumbnaildir . basename($File::Find::name);

	if(-e $thumbnailfile) {
	    $num_exist++;
	} else {
	    make_path($thumbnaildir, {
		verbose => 1,
		mode => 0755,
		      });
	    $num_to_create++;
	    makeThumbs($File::Find::name,$thumbnailfile);
	}

    }
}

find(\&processFiles, @ARGV);

print "$num_exist thumbnails existed, and $num_to_create needed to be created\n";

print "Copy the above URLs into emacs and do C-c ! on them.\n\n";

my $duration = time - $timestart;
print "Execution time: $duration s\n";
